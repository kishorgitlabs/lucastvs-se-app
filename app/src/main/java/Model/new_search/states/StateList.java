package Model.new_search.states;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
/**
 * Awesome Pojo Generator
 * */
public class StateList{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private List<String> data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(List<String> data){
   this.data=data;
  }
  public List<String> getData(){
   return data;
  }
}
package Alert;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tvs.R;


public class Alert {
    public Context context;
    public AlertDialog alertDialog;

    public Alert(Context context) {
        this.context = context;
    }


    //this method is show the alert with different classes.
    public void showAlertbox(String msg) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        TextView alert = dialogView.findViewById(R.id.alert_text);
        alert.setText(msg);
        Button okay = (Button)  dialogView.findViewById(R.id.okay);
        alertDialog.setView(dialogView);
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
            }
        });
        //alertDialog.setMessage(msg);
        //alertDialog.setTitle("Turbo Energy Limited");
       /* alertDialog.setIcon(ContextCompat.getDrawable(context,R.mipmap.ic_launcher_square));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });*/
        //alertDialog.setIcon(R.drawable.logo);
        alertDialog.show();
        /*alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        })*/
    }
}
